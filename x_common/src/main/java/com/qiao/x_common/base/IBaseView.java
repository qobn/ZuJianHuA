package com.qiao.x_common.base;

public interface IBaseView {
    void showLoading();
    void hideLoading();
}
