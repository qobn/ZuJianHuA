package com.qiao.x_common.Api;


import com.qiao.x_common.NetUtils.ServerUrl;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Administrator on 2018/9/25 0025.
 * 2、post请求
 * 注解里传入 网络请求 的部分URL地址
 * Retrofit把网络请求的URL分成了两部分：一部分放在Retrofit对象里，另一部分放在网络请求接口里
 * 如果接口里的url是一个完整的网址，那么放在Retrofit对象里的URL可以忽略
 * getCall()是接受网络请求数据的方法
 */

public interface ApiInterface {

//    @GET(ServerUrl.bookInfoUrl)
//    Call<BookInfo> getCall(@Query("id") String id, @Query("chapterid") String chapterid);
//
//    @FormUrlEncoded
//    @POST(ServerUrl.bookInfoUrl)
//    Call<BookInfo> postCall(@Field("id") String id, @Field("chapterid") String chapterid);
//
//    @FormUrlEncoded
//    @POST(ServerUrl.bookListUrl)
//    Call<BookList> getBookList(@Field("id") String id);
//
//    @POST(ServerUrl.bookMainUrl)
//    Call<BookMain> getBookMain();
//
//    @POST(ServerUrl.bookMainUrl)
//    Observable<BookMain> getBookMainRxjava();

    @POST(ServerUrl.bookMainUrl)
    Observable<ResponseBody> getBookMainRxjavaRespone();


    @FormUrlEncoded
    @POST(ServerUrl.bookListUrl)
    Observable<ResponseBody> getBookListOb(@Field("id") String id);
}
