package com.qiao.x_common.base;

import android.app.Activity;
import android.os.Bundle;

/**
 * %%       %%
 * *       ┏-*    ┏_┛ ┻---━┛_┻━━┓
 * ┃　　　        ┃
 * ┃　　 ━　      ┃
 * ┃ @^　  @^    ┃
 * ┃　　　　　　  ┃
 * ┃　　 ┻　　　 ┃
 * ┃_　　　　　 _┃
 * ┗━┓　　　┏━┛
 * 　　┃　　　┃神兽保佑
 * 　　┃　　　┃永无BUG！
 * 　　┃　　　┗━━━┓----|
 * 　　┃　　　　　　　  ┣┓}}}
 * 　　┃　　　　　　　  ┏┛
 * 　　┗┓&&&┓-┏&&&┓┏┛-|
 * 　　　┃┫┫　 ┃┫┫
 * 　　　┗┻┛　 ┗┻┛
 * 项目名称 ZuJianHua
 * 描述
 * 作者 Administrator
 * 时间 2019/11/18 17:18
 */
public class BaseActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
