package com.qiao.x_login;

import com.qiao.x_common.base.BaseApplication;

/**
 * %%       %%
 * *       ┏-*    ┏_┛ ┻---━┛_┻━━┓
 * ┃　　　        ┃
 * ┃　　 ━　      ┃
 * ┃ @^　  @^    ┃
 * ┃　　　　　　  ┃
 * ┃　　 ┻　　　 ┃
 * ┃_　　　　　 _┃
 * ┗━┓　　　┏━┛
 * 　　┃　　　┃神兽保佑
 * 　　┃　　　┃永无BUG！
 * 　　┃　　　┗━━━┓----|
 * 　　┃　　　　　　　  ┣┓}}}
 * 　　┃　　　　　　　  ┏┛
 * 　　┗┓&&&┓-┏&&&┓┏┛-|
 * 　　　┃┫┫　 ┃┫┫
 * 　　　┗┻┛　 ┗┻┛
 * 项目名称 ZuJianHua
 * 描述
 * 作者 Administrator
 * 时间 2019/11/18 17:54
 */
public class LoginApplication extends BaseApplication {
    @Override
    public void onCreate() {
        super.onCreate();
    }
}
