package com.qiao.x_common.base;

import android.app.Application;
import android.content.Context;


/**
 * %%       %%
 * *       ┏-*    ┏_┛ ┻---━┛_┻━━┓
 * ┃　　　        ┃
 * ┃　　 ━　      ┃
 * ┃ @^　  @^    ┃
 * ┃　　　　　　  ┃
 * ┃　　 ┻　　　 ┃
 * ┃_　　　　　 _┃
 * ┗━┓　　　┏━┛
 * 　　┃　　　┃神兽保佑
 * 　　┃　　　┃永无BUG！
 * 　　┃　　　┗━━━┓----|
 * 　　┃　　　　　　　  ┣┓}}}
 * 　　┃　　　　　　　  ┏┛
 * 　　┗┓&&&┓-┏&&&┓┏┛-|
 * 　　　┃┫┫　 ┃┫┫
 * 　　　┗┻┛　 ┗┻┛
 * 项目名称 ZuJianHua
 * 描述
 * 作者 Administrator
 * 时间 2019/11/18 17:18
 */
public class BaseApplication extends Application {
    public Context mContext;
    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
//        if (Utils.IS_LOG){
//            ARouter.openLog();     // 打印日志
//            ARouter.openDebug();
//        }
//        ARouter.init(this);
    }
}
