package com.qiao.x_common.Utils;


import android.util.Log;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

public class Utils {
    public static boolean IS_LOG = true;
    private static String log_tag = "ht";
    public static OkHttpClient getoKhttpClient() {

        long TIMEOUT = 30;
        //定制OkHttp
        OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder();
        if (IS_LOG) {//发布版本不再打印
            // 日志显示级别
            HttpLoggingInterceptor.Level level = HttpLoggingInterceptor.Level.BODY;
            //新建log拦截器
            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
                @Override
                public void log(String message) {
                    Log.e("test", "OkHttp====Message:" + message);
                }
            });
            loggingInterceptor.setLevel(level);
            //OkHttp进行添加拦截器loggingInterceptor
            httpClientBuilder.addInterceptor(loggingInterceptor);
            httpClientBuilder.connectTimeout(TIMEOUT,TimeUnit.SECONDS);
            httpClientBuilder.readTimeout(TIMEOUT,TimeUnit.SECONDS);
        }
        return httpClientBuilder.build();
    }
    public static void mLogError(String msg) {
        if (IS_LOG)
            Log.e(log_tag, msg);
    }

    public static void mLog(String msg) {
        if (IS_LOG)
            Log.i(log_tag, msg);
    }

    public static void mLog_d(String msg) {
        if (IS_LOG)
            Log.d(log_tag, msg);
    }

    public static void mLog_v(String msg) {
        if (IS_LOG)
            Log.v(log_tag, msg);
    }

    public static void mLog_w(String msg) {
        if (IS_LOG)
            Log.w(log_tag, msg);
    }

}
