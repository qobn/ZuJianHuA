package com.qiao.x_login;

import android.os.Bundle;

import com.qiao.x_common.base.BaseActivity;


/**
 * %%       %%
 * *       ┏-*    ┏_┛ ┻---━┛_┻━━┓
 * ┃　　　        ┃
 * ┃　　 ━　      ┃
 * ┃ @^　  @^    ┃
 * ┃　　　　　　  ┃
 * ┃　　 ┻　　　 ┃
 * ┃_　　　　　 _┃
 * ┗━┓　　　┏━┛
 * 　　┃　　　┃神兽保佑
 * 　　┃　　　┃永无BUG！
 * 　　┃　　　┗━━━┓----|
 * 　　┃　　　　　　　  ┣┓}}}
 * 　　┃　　　　　　　  ┏┛
 * 　　┗┓&&&┓-┏&&&┓┏┛-|
 * 　　　┃┫┫　 ┃┫┫
 * 　　　┗┻┛　 ┗┻┛
 * 项目名称 ZuJianHua
 * 描述
 * 作者 Administrator
 * 时间 2019/11/18 17:52
 */
public class LoginActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.x_login);
    }
}
