package com.qiao.x_common.Utils;


import com.qiao.x_common.NetUtils.ServerUrl;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;

public class CommUtils {

    //获取书籍列表
    public static void getBookListOb(String id,CallBack callBack){
        ServerUrl.getApiInterface().getBookListOb(id).subscribeOn(Schedulers.io()).doOnSubscribe(new Consumer<Disposable>() {
            @Override
            public void accept(Disposable disposable) throws Exception {

            }
        }).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<ResponseBody>() {
            @Override
            public void accept(ResponseBody responseBody) throws Exception {
                callBack.SuccessOk(responseBody.string());
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                callBack.ErrorOk(throwable.getMessage());
            }
        });
    }





    public interface CallBack{
        void SuccessOk(String data);

        void ErrorOk(String errorMsg);
    }
}
