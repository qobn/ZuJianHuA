package com.qiao.x_common.NetUtils;
import com.qiao.x_common.Api.ApiInterface;
import com.qiao.x_common.Utils.Utils;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Administrator on 2018/9/25 0025.
 * id/18_18014/chapterid/8089764/
 * http://www.zbook.io/v1/bookinfo?id=1_1000&chapterid=637850
 */

public class ServerUrl {//这个retrofit url 结尾必须 / 结尾

    private static int getEnvironmental() {
        return 2;//1.test环境---2.线上环境
    }

    /**
     * 获取带端口的IP地址
     *
     * @return
     */
    public static String getServerUrl() {//小说部分
        String url = "";
        switch (getEnvironmental()) {
            case 1://test环境
                url = "http://www.zbook.io/";
                break;
            case 2://线上环境
                url = "http://book.eatandshow.com/";
                break;
            default:
                break;
        }
        return url;
    }

    /**
     * 图片前缀
     * @return
     */
    public static String getServerImgUrl(){
        String url = "";
        switch (getEnvironmental()) {
            case 1://test环境
                url = "http://www.zbook.io/";
                break;
            case 2://线上环境
                url = "http://book.eatandshow.com/saveimgs";
                break;
            default:
                break;
        }
        return url;
    }

    public static final String bookMainUrl = "phoneapi.php/v1/bookmain/";
    public static final String bookListUrl = "phoneapi.php/v1/booklist/";
    public static final String bookInfoUrl = "phoneapi.php/v1/bookinfo/";



    public static ApiInterface getApiInterface() {
        //创建Retrofit对象
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ServerUrl.getServerUrl())
                .client(Utils.getoKhttpClient())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        //创建网络请求接口实例
        ApiInterface request_interface = retrofit.create(ApiInterface.class);
        return request_interface;
    }

}
